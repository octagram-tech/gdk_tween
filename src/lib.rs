pub mod tween {

    use keyframe::{ease, EasingFunction};
    use std::{collections::hash_map::HashMap, time::Instant};

    static mut TWEEN: Option<HashMap<String, Tween>> = None;

    unsafe fn get_tweens() -> &'static mut HashMap<String, Tween> {
        if TWEEN.is_none() {
            return TWEEN.get_or_insert(HashMap::new());
        } else {
            return TWEEN
                .as_mut()
                .expect("Error: Failed to get valid tween instance!");
        }
    }

    /// Get a tween object via id
    pub fn get_tween(id: &String) -> Option<&'static mut Tween> {
        unsafe { get_tweens() }.get_mut(id)
    }

    /// Destroy a tween
    pub fn kill_tween(id: &String) {
        unsafe {
            let tweens = get_tweens();

            if tweens.contains_key(id) {
                tweens.remove(id);
            }
        }
    }

    /// Tween an object from and to
    pub fn to<F: EasingFunction>(
        id: &String,
        ease_function: F,
        from: f32,
        to: f32,
        delta: f32,
    ) -> Option<f32> {
        let tween = get_tween(id);
        if tween.is_some() {
            return Some(tween.unwrap().to(ease_function, from, to, delta));
        }

        None
    }

    /// Insert a new tween object
    pub fn new(id: String, tween: Tween) {
        unsafe { get_tweens() }.insert(id, tween);
    }

    /// Update all tween objects
    pub fn update() {
        let tweens = unsafe { get_tweens() };

        if tweens.len() > 0 {
            tweens.retain(|_, tween| {
                if !tween.is_finished() {
                    return true;
                } else {
                    if !tween.is_kill_on_complete() {
                        return true;
                    }
                }
                return false;
            });
        }
    }

    /// Tween configuration data
    #[derive(Debug, Clone, Copy)]
    pub struct TweenConfig {
        pub duration: f32,
        pub yoyo: bool,
        pub repeat_count: i8,
        pub kill_on_complete: bool,
        pub delay: f32,
    }

    impl Default for TweenConfig {
        fn default() -> Self {
            TweenConfig {
                duration: 1.,
                repeat_count: 0,
                yoyo: false,
                kill_on_complete: true,
                delay: 0.,
            }
        }
    }

    /// Tween object
    #[derive(Debug)]
    pub struct Tween {
        progress: f32,
        reverse: bool,
        yoyo_cycle_count: u8,
        finished: bool,
        config: TweenConfig,
        cycles: u8,
        delay_timer: Option<Instant>,
        delay_finished: bool,
    }

    impl Tween {
        /// Create a new tween object
        pub fn new(config: TweenConfig) -> Tween {
            Tween {
                progress: 0.,
                config,
                finished: false,
                reverse: false,
                yoyo_cycle_count: 0,
                cycles: 0,
                delay_timer: None,
                delay_finished: config.delay == 0.,
            }
        }

        fn is_repeat(&self) -> bool {
            let repeat_count = self.config.repeat_count;
            repeat_count == -1 || repeat_count > 0
        }

        fn is_repeat_infinite(&self) -> bool {
            self.config.repeat_count == -1
        }

        fn is_yoyo_full_cycle(&self) -> bool {
            self.yoyo_cycle_count > 0 && self.yoyo_cycle_count % 2 == 0
        }

        /// Current progress between 0 - 1
        pub fn progress(&self) -> f32 {
            self.progress
        }

        /// Reset the tween
        pub fn reset(&mut self) {
            self.reverse = false;
            self.finished = false;
            self.progress = 0.;
            self.yoyo_cycle_count = 0;
            self.cycles = 0;
            self.delay_timer = None;
            self.delay_finished = self.config.delay == 0.;
        }

        /// Rewind the tween
        pub fn rewind(&mut self) {
            if self.finished {
                self.reverse = !self.reverse;
                self.progress = 0.;
                self.yoyo_cycle_count = 0;
                self.finished = false;
                self.delay_timer = None;
                self.delay_finished = self.config.delay == 0.;
            }
        }

        /// Whether or not the tween has finished
        pub fn is_finished(&self) -> bool {
            self.finished
        }

        /// Whether or not the tween is being killed on completion
        pub fn is_kill_on_complete(&self) -> bool {
            self.config.kill_on_complete
        }

        /// Set the duration value
        pub fn set_duration(&mut self, value: f32) {
            self.config.duration = value;
        }

        /// Set the delay value
        pub fn set_delay(&mut self, value: f32) {
            self.config.delay = value;
        }

        /// Tween the  object from and to
        pub fn to<F: EasingFunction>(
            &mut self,
            ease_function: F,
            from: f32,
            to: f32,
            delta: f32,
        ) -> f32 {
            let config = &self.config;
            let delay = config.delay;
            let from_to: (f32, f32) = if self.reverse { (to, from) } else { (from, to) };
            let mut value = ease(ease_function, from_to.0, from_to.1, self.progress);

            if !self.delay_finished {
                if self.delay_timer.is_none() {
                    self.delay_timer = Some(Instant::now());
                }

                if self.delay_timer.unwrap().elapsed().as_millis() > (delay * 1000.) as u128 {
                    self.delay_finished = true;
                }
            } else {
                if self.progress >= 1. {
                    self.progress = 1.;

                    if config.repeat_count > 0 {
                        if self.cycles as i8 >= config.repeat_count {
                            if config.yoyo {
                                if !self.is_yoyo_full_cycle() {
                                    self.finished = true;
                                }
                            } else {
                                self.finished = true;
                            }
                        }
                    } else {
                        if !self.is_repeat_infinite() {
                            if config.yoyo {
                                if !self.is_yoyo_full_cycle() && self.yoyo_cycle_count == 1 {
                                    self.finished = true;
                                }
                            } else {
                                self.finished = true;
                            }
                        }
                    }
                    if !self.finished {
                        if config.yoyo {
                            self.reverse = !self.reverse;
                            self.progress = 0.;
                            self.yoyo_cycle_count += 1;
                            if self.is_yoyo_full_cycle() {
                                self.cycles += 1;
                            }
                        }
                        if self.is_repeat() {
                            self.progress = 0.;
                            if !config.yoyo {
                                value = from;
                                self.cycles += 1;
                            }
                        }
                    }
                } else {
                    self.progress += delta / config.duration;
                }
            }

            value
        }
    }
}
